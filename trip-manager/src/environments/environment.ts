// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBbj1cyTyXT0JXP8KDaDutmn8DK2nYOfLU',
    authDomain: 'my-travel-manager-project.firebaseapp.com',
    databaseURL: 'https://my-travel-manager-project.firebaseio.com',
    projectId: 'my-travel-manager-project',
    storageBucket: 'my-travel-manager-project.appspot.com',
    messagingSenderId: '249248954623',
  },
  firebase_api: {
    apiKey: 'AIzaSyBbj1cyTyXT0JXP8KDaDutmn8DK2nYOfLU',
    clientId:
      '249248954623-4mst75nqos6q26fegi0b873fk3kv7h3m.apps.googleusercontent.com',
    discoveryDocs: [
      'https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest',
    ],
    scope: [
      'https://www.googleapis.com/auth/calendar',
      'https://www.googleapis.com/auth/drive',
    ].join(' '),
  },
  routes: {
    auth: 'auth',
    login: 'auth/login',
    register: 'auth/register',
    profile: 'profile',
    trip_add: 'trip',
  },
  current_user: 'current-user',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
