import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppConfig } from '../configs/app.config';

const authRoutes = [
  { path: '', redirectTo: AppConfig.routes.login },
  { path: AppConfig.routes.login, component: LoginComponent },
  { path: AppConfig.routes.register, component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(authRoutes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
