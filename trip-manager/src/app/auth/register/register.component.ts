import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AppConfig } from 'src/app/configs/app.config';
import { AuthService } from '../../shared/services/auth.service';
import { emailValidator } from './../../shared/directives/validate.email.directive';
import { emailConfirmValidator } from 'src/app/shared/directives/confirm.email.directive';

@Component({
  selector: 'app-auth-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  public registerForm: FormGroup;
  public hide = false;
  public email = new FormControl('', [Validators.required, emailValidator]);
  public confirmEmail = new FormControl('', [
    Validators.required,
    emailConfirmValidator('email'),
  ]);

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.createForm();
  }

  createForm() {
    this.registerForm = this.fb.group({
      email: this.email,
      password: ['', Validators.required],
      name: ['', Validators.required],
      confirmEmail: this.confirmEmail,
    });
  }

  doRegister() {
    this.authService
      .doRegister(
        this.registerForm.value.email,
        this.registerForm.value.password,
        this.registerForm.value.name,
      )
      .subscribe(() => {
        return this.router.navigate(['']);
      });
  }

  getEmailErrorMessage() {
    return this.email.hasError('required')
      ? 'You must enter a value.'
      : this.email.hasError('invalidEmail')
      ? this.email.errors.invalidEmail
      : '';
  }

  getConfirmEmailErrorMessage() {
    return this.confirmEmail.hasError('required')
      ? 'You must enter a value.'
      : this.confirmEmail.hasError('matchEmail')
      ? this.confirmEmail.errors.matchEmail
      : '';
  }

  navigateLogin() {
    this.router.navigate([AppConfig.routes.auth_login]);
  }
}
