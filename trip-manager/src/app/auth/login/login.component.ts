import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { AppConfig } from './../../configs/app.config';
import { emailValidator } from './../../shared/directives/validate.email.directive';

@Component({
  selector: 'app-auth-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public hide = true;
  public email = new FormControl('', [Validators.required, emailValidator]);

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  createForm() {
    this.loginForm = this.fb.group({
      email: this.email,
      password: ['', Validators.required],
    });
  }

  getEmailErrorMessage() {
    return this.email.hasError('required')
      ? 'You must enter a value.'
      : this.email.hasError('invalidEmail')
      ? this.email.errors.invalidEmail
      : '';
  }

  doLogin() {
    this.authService
      .doLogin(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(() => {
        return this.router.navigate(['']);
      });
  }

  navigateRegister() {
    this.router.navigate([AppConfig.routes.auth_register]);
  }
}
