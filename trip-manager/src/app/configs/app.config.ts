export const AppConfig: any = {
  routes: {
    auth: 'auth',
    auth_login: 'auth/login',
    auth_register: 'auth/register',
    login: 'login',
    register: 'register',
    profile: 'profile',
    trip_add: 'trips',
    trip_view: 'trips/view/',
    trip_edit: '/trips/edit/',
  },
  db: {
    trips: 'trips',
    images: 'images',
    flights: 'flights',
    hotels: 'hotels',
  },
};
