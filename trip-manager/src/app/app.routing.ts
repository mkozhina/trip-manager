import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppConfig } from './configs/app.config';
import { HomePageComponent } from './shared/components/home-page/home-page.component';
import { AuthGuardService as AuthGuard } from './shared/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: AppConfig.routes.profile,
    loadChildren: './modules/profile/profile.module#ProfileModule',
    canActivate: [AuthGuard],
  },
  {
    path: AppConfig.routes.auth,
    loadChildren: './auth/auth.module#AuthModule',
  },
  {
    path: 'trips',
    loadChildren: './modules/trip/trip.module#TripModule',
    canActivate: [AuthGuard],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
