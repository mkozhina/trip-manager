import { FlightStore } from './dialogs/flights/state/flight.store';
import { TripRoutingModule } from './trip.routing';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TripEditComponent } from './trip-edit/trip-edit.component';
import { TripViewComponent } from './trip-view/trip-view.component';
import { TripListComponent } from './trip-list/trip-list.component';
import { TripService } from './trip.service';
import { TripsCardViewComponent } from './trips-card-view/trips-card-view.component';
import { TripCalendarComponent } from './components/trip-calendar/trip-calendar.component';
import { TripCalendarItemComponent } from './components/trip-calendar-item/trip-calendar-item.component';
import { TripBoardComponent } from './components/trip-board/trip-board.component';
import { MenuPlansComponent } from './components/menu-plans/menu-plans.component';
import { FlightDialogComponent } from './dialogs/flights/flight-dialog/flight-dialog.component';
import { FlightService } from './dialogs/flights/state/flight.service';
import { FlightViewComponent } from './dialogs/flights/flight-view/flight-view.component';
import { DeleteDialogComponent } from './dialogs/delete-dialog/delete-dialog.component';
import { HotelDialogComponent } from './dialogs/hotels/hotel-dialog/hotel-dialog.component';
import { HotelViewComponent } from './dialogs/hotels/hotel-view/hotel-view.component';
import { HotelService } from './dialogs/hotels/state/hotel.service';

@NgModule({
  declarations: [
    TripEditComponent,
    TripViewComponent,
    TripListComponent,
    TripsCardViewComponent,
    TripCalendarComponent,
    TripCalendarItemComponent,
    TripBoardComponent,
    MenuPlansComponent,
    FlightDialogComponent,
    FlightViewComponent,
    DeleteDialogComponent,
    HotelDialogComponent,
    HotelViewComponent,
  ],
  imports: [CommonModule, SharedModule, TripRoutingModule],
  providers: [TripService, FlightService, HotelService],
  entryComponents: [
    FlightDialogComponent,
    DeleteDialogComponent,
    HotelDialogComponent,
  ],
})
export class TripModule {}
