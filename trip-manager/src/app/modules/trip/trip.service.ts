import { NotificationService } from './../../shared/services/notification.service';
import { AppConfig } from 'src/app/configs/app.config';
import { AuthService } from './../../shared/services/auth.service';
import { Trip } from './trip.model';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DocumentReference, AngularFirestore } from '@angular/fire/firestore';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';

@Injectable()
export class TripService {
  constructor(
    private afs: AngularFirestore,
    public authService: AuthService,
    public notify: NotificationService,
    private router: Router,
  ) {}

  getTrips(): Observable<Trip[]> {
    return this.afs
      .collection<Trip>(AppConfig.db.trips, ref => {
        return ref.where('uid', '==', this.authService.getCurrentUserID());
      })
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(action => {
            const data = action.payload.doc.data();
            data.startDate = formatDate(data.startDate, 'MMMM d, y', 'en-US');
            data.endDate = formatDate(data.endDate, 'MMMM d, y', 'en-US');
            return new Trip({
              id: action.payload.doc.id,
              ...data,
            });
          });
        }),
        catchError(err => throwError(err)),
      );
  }

  getTrip(id: string): Observable<any> {
    return this.afs
      .doc(AppConfig.db.trips + '/' + id)
      .get()
      .pipe(
        map(trip => {
          return new Trip({ id, ...trip.data() });
        }),
        catchError(err => throwError(err)),
      );
  }

  createTrip(trip: Trip): Observable<DocumentReference> {
    trip.uid = this.authService.getCurrentUserID();
    return from(
      this.afs
        .collection<Trip>(AppConfig.db.trips)
        .add(JSON.parse(JSON.stringify(trip)))
        .then(res => {
          this.router.navigate([AppConfig.routes.trip_view + res.id]);
          return res;
        }),
    ).pipe(catchError(err => throwError(err)));
  }

  updateTrip(trip: Trip) {
    trip.uid = this.authService.getCurrentUserID();
    return from(
      this.afs
        .doc(`${AppConfig.db.trips}/${trip.id}`)
        .update(JSON.parse(JSON.stringify(trip)))
        .then(() => this.notify.success('Updated trip.')),
    ).pipe(
      catchError(err => {
        return throwError(err);
      }),
    );
  }
}
