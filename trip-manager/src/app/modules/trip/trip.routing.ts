import { TripListComponent } from './trip-list/trip-list.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TripEditComponent } from './trip-edit/trip-edit.component';
import { TripViewComponent } from './trip-view/trip-view.component';

const routes = [
  { path: '', component: TripListComponent },
  { path: 'view/:id', component: TripViewComponent },
  { path: 'edit/:id', component: TripEditComponent },
  { path: 'add', redirectTo: 'edit/' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TripRoutingModule {}
