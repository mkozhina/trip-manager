import {
  TripViewList,
  ActivePeriod,
  Plans,
} from './../../../shared/models/trip-view.model';
import { HotelService } from './../dialogs/hotels/state/hotel.service';
import { HotelQuery } from './../dialogs/hotels/state/hotel.query';
import { DateFormaterService } from './../../../shared/services/date-formater.service';
import { FlightService } from './../dialogs/flights/state/flight.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TripService } from '../trip.service';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Trip } from '../trip.model';
import { FlightQuery } from '../dialogs/flights/state/flight.query';
import Utils from 'src/app/shared/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-trip-view',
  templateUrl: './trip-view.component.html',
  styleUrls: ['./trip-view.component.scss'],
})
export class TripViewComponent implements OnInit, OnDestroy {
  public subscriptions = new Subscription();
  public tripViewModel: TripViewList;
  private tripID: string;

  constructor(
    private tripService: TripService,
    private activatedRoute: ActivatedRoute,
    private flightQuery: FlightQuery,
    private hotelQuery: HotelQuery,
    private hotelService: HotelService,
    private flightService: FlightService,
    private dateUtil: DateFormaterService,
  ) {}

  ngOnInit() {
    this.tripViewModel = new TripViewList();
    this.tripID = this.activatedRoute.snapshot.paramMap.get('id');
    this.initViewModel();
  }

  initViewModel() {
    this.subscriptions.add(
      this.tripService.getTrip(this.tripID).subscribe(trip => {
        if (trip) {
          this.tripViewModel.trip = trip;
          const activePeriod = this.initDatePeriod(trip);
          this.tripViewModel.activePeriod = activePeriod;
          if (activePeriod && activePeriod.length > 0) {
            this.initFlights(activePeriod);
            this.initHotels(activePeriod);
          }
        }
      }),
    );
  }

  initDatePeriod(trip: Trip): ActivePeriod[] {
    const activePeriod: ActivePeriod[] = [];
    const start = moment(trip.startDate);
    const end = moment(trip.endDate);
    let day = start;
    while (day <= end) {
      activePeriod.push(new ActivePeriod(Utils.getRandomId(), day, null));
      day = day.clone().add(1, 'd');
    }
    return activePeriod;
  }

  initFlights(activePeriod: ActivePeriod[]) {
    this.flightService.fetch();
    this.subscriptions.add(
      this.flightQuery.selectAll().subscribe(flights => {
        if (flights) {
          activePeriod.forEach(p => {
            const filtered = flights.filter(f => {
              const dateFormated = moment(p.day).format('MM/DD/YYYY');
              return (
                f.tripID === this.tripID &&
                this.dateUtil.formatDate(f.depDate) === dateFormated
              );
            });

            if (filtered) {
              if (!p.plans) {
                p.plans = new Plans();
              }
              p.plans.flights = filtered;
            }
          });
        }
      }),
    );
  }

  initHotels(activePeriod: ActivePeriod[]) {
    this.hotelService.fetch();
    this.subscriptions.add(
      this.hotelQuery.selectAll().subscribe(hotels => {
        if (hotels) {
          activePeriod.forEach(p => {
            const filtered = hotels.filter(f => {
              const dateFormated = moment(p.day).format('MM/DD/YYYY');
              return (
                (f.tripID === this.tripID &&
                  this.dateUtil.formatDate(f.checkInDate) === dateFormated) ||
                this.dateUtil.formatDate(f.checkOutDate) === dateFormated
              );
            });

            if (filtered) {
              if (!p.plans) {
                p.plans = new Plans();
              }

              p.plans.hotels = filtered;
            }
          });
        }
      }),
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this.flightService.destroy();
    this.hotelService.destroy();
    this.tripViewModel = null;
  }
}
