import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsCardViewComponent } from './trips-card-view.component';

describe('TripsCardViewComponent', () => {
  let component: TripsCardViewComponent;
  let fixture: ComponentFixture<TripsCardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripsCardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
