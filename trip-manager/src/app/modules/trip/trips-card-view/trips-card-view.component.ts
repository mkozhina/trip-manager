import { ViewMode } from './../../../shared/enums/view-mode.enum';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Trip } from '../trip.model';

@Component({
  selector: 'app-trips-card-view',
  templateUrl: './trips-card-view.component.html',
  styleUrls: ['./trips-card-view.component.scss'],
})
export class TripsCardViewComponent implements OnInit, OnChanges {
  @Input() tripList: Trip[];
  @Input() mode: ViewMode;
  public tripsView: Array<{}> = [];
  public isTableView = false;
  constructor() {}

  ngOnInit() {
    this.renderTripView();
  }

  ngOnChanges() {
    this.ngOnInit();
  }

  renderTripView() {
    this.tripsView = [];
    this.isTableView = +this.mode === ViewMode.table;
    const cols = +this.mode === ViewMode.table ? 2 : 1;
    const rows = 1;

    this.tripList.forEach(t => {
      this.tripsView.push({ cols, rows, ...t });
    });
  }
}
