import { ViewMode } from "./../../../shared/enums/view-mode.enum";
import { Component, OnInit } from "@angular/core";
import { TripService } from "../trip.service";
import { Trip } from "../trip.model";
import { Observable } from "rxjs";

@Component({
  selector: "app-trip-list",
  templateUrl: "./trip-list.component.html",
  styleUrls: ["./trip-list.component.scss"]
})
export class TripListComponent implements OnInit {
  ViewMode = ViewMode;
  public viewModeValue: ViewMode = ViewMode.card;
  public trips$: Observable<Trip[]>;
  constructor(private tripService: TripService) {}

  ngOnInit() {
    this.trips$ = this.tripService.getTrips();
  }
}
