import { GoogleCalendarService } from './../../../shared/services/google-calendar.service.ts.service';
import * as dialogMarkerComponent from './../../../shared/components/map/dialog-marker/dialog-marker.component';
import { Location } from '../../../shared/models/location.model';
import { NotificationService } from './../../../shared/services/notification.service';
import { TripService } from '../trip.service';
import { Trip } from '../trip.model';
import { ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import { MatDialog } from '@angular/material';
import {} from 'googlemaps';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup,
} from '@angular/forms';
import {
  Component,
  OnInit,
  ViewChild,
  NgZone,
  AfterViewInit,
} from '@angular/core';

@Component({
  selector: 'app-trip-edit',
  templateUrl: './trip-edit.component.html',
  styleUrls: ['./trip-edit.component.scss'],
})
export class TripEditComponent implements OnInit, AfterViewInit {
  public tripEditForm: FormGroup;
  public location: Location = new Location();
  private id = this.activatedRoute.snapshot.paramMap.get('id');
  public isAddForm = !this.id;
  @ViewChild('addresstext') addresstext: any;

  constructor(
    private tripService: TripService,
    public notify: NotificationService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    public zone: NgZone,
    public dialog: MatDialog,
    public calendar: GoogleCalendarService,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.initTrip();
  }

  ngAfterViewInit() {
    this.getPlaceAutocomplete();
  }

  exportToCalendar() {
    this.calendar.addEventToCalendar(
      this.tripEditForm.value.startDate,
      this.tripEditForm.value.endDate,
      this.tripEditForm.value.name,
    );
  }

  initTrip() {
    if (!this.isAddForm) {
      this.tripService.getTrip(this.id).subscribe((trip: Trip) => {
        const tripObj = trip;
        this.createForm(tripObj);
      });
    } else {
      this.createForm();
      this.setCurrentLocation();
    }
  }

  setCurrentLocation() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.setLocation(+pos.coords.latitude, +pos.coords.longitude, '');
      });
    }
  }

  openDialog(lat: number, lng: number): void {
    const dialogRef = this.dialog.open(
      dialogMarkerComponent.DialogMarkerComponent,
      {
        width: '250px',
        data: { destination: '' },
      },
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setLocation(lat, lng, result);
        this.addresstext.nativeElement.value = result;
        this.tripEditForm.patchValue({
          destination: result,
        });
      }
    });
  }

  setLocation(lat: number, lng: number, name: string) {
    this.location.lat = lat;
    this.location.lng = lng;
    this.location.name = name;
  }

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(
      this.addresstext.nativeElement,
      {
        types: ['(cities)'],
      },
    );
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();

      this.setLocation(
        place.geometry.location.lat(),
        place.geometry.location.lng(),
        place.formatted_address,
      );
      this.tripEditForm.patchValue({
        destination: place.formatted_address,
      });
    });
  }

  updateTrip() {
    if (this.tripEditForm.valid) {
      const tripObj = new Trip({
        destLocation: new firebase.firestore.GeoPoint(
          this.location.lat,
          this.location.lng,
        ),
        id: this.id,
        ...this.tripEditForm.value,
      });
      this.tripService
        .updateTrip(tripObj)
        .subscribe(() => {}, err => this.notify.error(err.message));
    } else {
      this.notify.error('Fill required fields.');
    }
  }

  createTrip() {
    if (this.tripEditForm.valid) {
      const tripObj = new Trip({
        destLocation: new firebase.firestore.GeoPoint(
          this.location.lat,
          this.location.lng,
        ),
        ...this.tripEditForm.value,
      });
      this.tripService.createTrip(tripObj).subscribe();
    }
  }

  createForm(trip: Trip = null) {
    if (trip) {
      const destData = trip.destLocation;
      this.setLocation(destData._lat, destData._long, trip.destination);
    }
    this.tripEditForm = this.fb.group({
      name: new FormControl(trip ? trip.name : '', [Validators.required]),
      destination: new FormControl(
        trip ? trip.destination : this.location.name,
      ),
      startDate: new FormControl(trip ? trip.startDate : '', [
        Validators.required,
      ]),
      endDate: new FormControl(trip ? trip.endDate : '', [Validators.required]),
    });
  }
}
