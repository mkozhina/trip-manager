import { Deserializable } from './../../shared/interfaces/deserializable.interface';
export class Trip implements Deserializable {
  id: string;
  name: string;
  uid: any;
  startDate: any;
  destLocation: any;
  endDate: any;
  destination: string;

  constructor(trip: any = {}) {
    this.id = trip.id;
    this.name = trip.name || '';
    this.uid = trip.uid;
    this.destLocation = trip.destLocation || null;
    this.destination = trip.destination || '';
    this.startDate = trip.startDate;
    this.endDate = trip.endDate;
  }

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
