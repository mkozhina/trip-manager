import { FlightDialogComponent } from '../../dialogs/flights/flight-dialog/flight-dialog.component';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Trip } from '../../trip.model';
import { HotelDialogComponent } from '../../dialogs/hotels/hotel-dialog/hotel-dialog.component';

@Component({
  selector: 'app-menu-plans',
  templateUrl: './menu-plans.component.html',
  styleUrls: ['./menu-plans.component.scss'],
})
export class MenuPlansComponent implements OnInit {
  @Input() trip: Trip;
  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  flightDialog(): void {
    this.dialog.open(FlightDialogComponent, {
      width: '1000px',
      data: { trip: this.trip },
    });
  }

  hotelDialog(): void {
    this.dialog.open(HotelDialogComponent, {
      width: '1000px',
      data: { trip: this.trip },
    });
  }
}
