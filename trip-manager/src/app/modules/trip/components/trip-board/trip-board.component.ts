import { TripViewList } from './../../../../shared/models/trip-view.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-trip-board',
  templateUrl: './trip-board.component.html',
  styleUrls: ['./trip-board.component.scss'],
})
export class TripBoardComponent implements OnInit {
  @Input() data: TripViewList;
  constructor() {}

  ngOnInit() {}

  hasPlansCheck(id: string): boolean {
    const periodItem = this.data.activePeriod.find(p => p.id === id);
    if (!periodItem || !periodItem.plans) {
      return false;
    }

    if (
      periodItem.plans.flights.length === 0 &&
      periodItem.plans.hotels.length === 0
    ) {
      return false;
    }
    return true;
  }
}
