import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripCalendarItemComponent } from './trip-calendar-item.component';

describe('TripCalendarItemComponent', () => {
  let component: TripCalendarItemComponent;
  let fixture: ComponentFixture<TripCalendarItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripCalendarItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripCalendarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
