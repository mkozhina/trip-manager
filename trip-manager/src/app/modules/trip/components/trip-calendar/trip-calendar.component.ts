import { TripViewList } from './../../../../shared/models/trip-view.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-trip-calendar',
  templateUrl: './trip-calendar.component.html',
  styleUrls: ['./trip-calendar.component.scss'],
})
export class TripCalendarComponent implements OnInit {
  @Input() data: TripViewList;

  constructor() {}

  ngOnInit() {}

  scroll(id) {
    const prev = document.querySelector('.activeBoard');
    if (prev) {
      prev.classList.remove('activeBoard');
    }

    const element = document.getElementById(id);
    element.classList.add('activeBoard');
    element.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
    });
  }

  hasPlansCheck(id: string): boolean {
    const periodItem = this.data.activePeriod.find(p => p.id === id);
    if (!periodItem || !periodItem.plans) {
      return false;
    }

    if (
      periodItem.plans.flights.length === 0 &&
      periodItem.plans.hotels.length === 0
    ) {
      return false;
    }
    return true;
  }
}
