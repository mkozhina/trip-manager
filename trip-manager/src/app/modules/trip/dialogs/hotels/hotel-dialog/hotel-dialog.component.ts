import { DateFormaterService } from './../../../../../shared/services/date-formater.service';
import { HotelService } from './../state/hotel.service';
import { Hotel } from './../state/hotel.model';
import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Trip } from '../../../trip.model';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { Location } from '../../../../../shared/models/location.model';
import * as firebase from 'firebase';

@Component({
  selector: 'app-hotel-dialog',
  templateUrl: './hotel-dialog.component.html',
  styleUrls: ['./hotel-dialog.component.scss'],
})
export class HotelDialogComponent implements OnInit, AfterViewInit {
  public hotelForm: FormGroup;
  public locationLngLat: Location = new Location();
  public editHotel = false;
  @ViewChild('addresstext') addresstext: any;
  constructor(
    public dialogRef: MatDialogRef<HotelDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { trip: Trip; hotel: Hotel },
    private fb: FormBuilder,
    private hotelService: HotelService,
    private dateUtil: DateFormaterService,
  ) {}

  ngOnInit() {
    this.createForm(this.data.hotel);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngAfterViewInit() {
    this.getPlaceAutocomplete();
  }
  setCurrentLocation() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.setLocation(+pos.coords.latitude, +pos.coords.longitude, '');
      });
    }
  }
  setLocation(lat: number, lng: number, name: string) {
    this.locationLngLat.lat = lat;
    this.locationLngLat.lng = lng;
    this.locationLngLat.name = name;
  }
  createForm(hotel: Hotel = null) {
    if (hotel) {
      this.editHotel = true;
      const addressData = hotel.addressLocation;
      if (addressData) {
        this.setLocation(addressData._lat, addressData._long, hotel.address);
      }
    } else {
      this.setCurrentLocation();
    }

    this.hotelForm = this.fb.group({
      guestName: new FormControl(hotel ? hotel.guestName : ''),
      guestNumber: new FormControl(hotel ? hotel.guestNumber : ''),
      location: new FormControl(hotel ? hotel.location : ''),
      address: new FormControl(hotel ? hotel.address : ''),
      roomNumber: new FormControl(hotel ? hotel.roomNumber : ''),
      checkInDate: new FormControl(
        hotel
          ? new Date(this.dateUtil.formatDate(hotel.checkInDate))
          : new Date(this.data.trip.startDate),
      ),
      checkInTime: new FormControl(hotel ? hotel.checkInTime : ''),
      checkOutDate: new FormControl(
        hotel
          ? new Date(this.dateUtil.formatDate(hotel.checkOutDate))
          : new Date(this.data.trip.startDate),
      ),
      checkOutTime: new FormControl(hotel ? hotel.checkOutTime : ''),
    });
  }

  submit() {
    const addressLocation = new firebase.firestore.GeoPoint(
      this.locationLngLat.lat,
      this.locationLngLat.lng,
    );

    if (this.data.hotel && this.data.hotel.id) {
      const id = this.data.hotel.id.toString();
      const obj = { id, addressLocation, ...this.hotelForm.value };
      this.dialogRef.close(obj);
    } else {
      const tripId = this.data.trip.id;
      const obj = { tripID: tripId, ...this.hotelForm.value };
      this.hotelService.addItem(obj);
      this.dialogRef.close();
    }
  }

  private getPlaceAutocomplete() {
    const autocompleteAddress = new google.maps.places.Autocomplete(
      this.addresstext.nativeElement,
      {
        types: ['address'],
      },
    );
    google.maps.event.addListener(autocompleteAddress, 'place_changed', () => {
      const place = autocompleteAddress.getPlace();
      if (place) {
        this.setLocation(
          place.geometry.location.lat(),
          place.geometry.location.lng(),
          place.formatted_address,
        );
        this.hotelForm.patchValue({
          address: place.formatted_address,
        });
      }
    });
  }

  dateFilter = (d: Date): boolean => {
    if (this.data.trip) {
      const start = moment(this.data.trip.startDate);
      const end = moment(this.data.trip.endDate);
      const dFormated = moment(d);
      return dFormated >= start && dFormated <= end;
    }
    return true;
  };
}
