import { DateFormaterService } from './../../../../../shared/services/date-formater.service';
import { HotelService } from './../state/hotel.service';
import { HotelDialogComponent } from './../hotel-dialog/hotel-dialog.component';
import { DeleteDialogComponent } from './../../delete-dialog/delete-dialog.component';
import { Component, OnInit, Input } from '@angular/core';
import { Hotel } from '../state/hotel.model';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-hotel-view',
  templateUrl: './hotel-view.component.html',
  styleUrls: ['./hotel-view.component.scss'],
})
export class HotelViewComponent implements OnInit {
  @Input() hotel: Hotel;
  @Input() day: string;
  public isCheckOutDate = false;
  constructor(
    private hotelService: HotelService,
    public dialog: MatDialog,
    private dateUtil: DateFormaterService,
  ) {}

  ngOnInit() {
    if (this.day) {
      const dayFormated = moment(this.day).format('MM/DD/YYYY');
      this.isCheckOutDate =
        this.dateUtil.formatDate(this.hotel.checkOutDate) === dayFormated;
    }
  }

  delete(id: any) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: id,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.hotelService.delete(result);
      }
    });
  }

  edit(id): void {
    const dialogRef = this.dialog.open(HotelDialogComponent, {
      width: '1000px',
      data: { hotel: this.hotel },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.hotelService.update(result.id, { ...result });
      }
    });
  }
}
