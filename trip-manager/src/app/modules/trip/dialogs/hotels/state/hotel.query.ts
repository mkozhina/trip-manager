import { Injectable } from '@angular/core';
import { Query, QueryEntity } from '@datorama/akita';
import { HotelStore, HotelState } from './hotel.store';
import { Hotel } from './hotel.model';

@Injectable({ providedIn: 'root' })
export class HotelQuery extends QueryEntity<HotelState, Hotel> {
  constructor(protected store: HotelStore) {
    super(store);
  }
}
