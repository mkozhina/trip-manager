import { AppConfig } from 'src/app/configs/app.config';
import { Hotel, createHotel } from './hotel.model';
import { Injectable } from '@angular/core';
import { HotelStore } from './hotel.store';
import {
  AngularFirestoreCollection,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Injectable()
export class HotelService {
  public subscriptions = new Subscription();
  public hotelsCollection: AngularFirestoreCollection;

  constructor(
    private afs: AngularFirestore,
    private hotelStore: HotelStore,
    private notify: NotificationService,
  ) {
    this.hotelsCollection = afs.collection(AppConfig.db.hotels);
    this.fetch();
  }

  fetch() {
    this.subscriptions.add(
      this.hotelsCollection.valueChanges().subscribe((hotels: Hotel[]) => {
        this.hotelStore.set(hotels);
      }),
    );
  }

  getItem(id: string) {
    this.fetch();
    this.hotelStore.setActive(id);
  }

  addItem(item: Hotel) {
    const id = this.afs.createId();
    item.id = id;
    this.hotelsCollection
      .doc(id)
      .set(item)
      .then(res => {
        this.hotelStore.add(createHotel(item));
      })
      .catch(err => this.notify.error(err.message));
  }

  delete(id: string) {
    this.hotelsCollection
      .doc(id)
      .delete()
      .then(res => {
        this.hotelStore.remove(id);
        this.notify.success('Deleted');
      });
  }

  update(id: string, item: Hotel) {
    this.hotelsCollection
      .doc(id)
      .update({ ...item })
      .then(res => {
        this.hotelStore.update(id, { ...item });
        this.notify.success('Hotel updated');
      })
      .catch(err => this.notify.error(err.message));
  }

  destroy() {
    this.subscriptions.unsubscribe();
  }
}
