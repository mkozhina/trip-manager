import { ID } from '@datorama/akita';

export interface Hotel {
  id: ID;
  tripID: string;
  guestName: string;
  guestNumber: number;
  location: string;
  address: string;
  roomNumber: number;
  checkInDate: string;
  checkInTime: string;
  checkOutDate: string;
  checkOutTime: string;
  addressLocation: any;
}

/**
 * A factory function that creates Hotel
 */
export function createHotel({
  id,
  tripID,
  guestName,
  guestNumber,
  location,
  address,
  roomNumber,
  checkInDate,
  checkInTime,
  checkOutDate,
  checkOutTime,
  addressLocation,
}): Hotel {
  return {
    id,
    tripID,
    guestName,
    guestNumber,
    location,
    address,
    roomNumber,
    checkInDate,
    checkInTime,
    checkOutDate,
    checkOutTime,
    addressLocation,
  };
}
