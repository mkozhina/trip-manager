import { Hotel } from './hotel.model';
import { Injectable } from '@angular/core';
import { StoreConfig, EntityStore, EntityState } from '@datorama/akita';

export interface HotelState extends EntityState<Hotel> {}
@Injectable({
  providedIn: 'root',
})
@StoreConfig({ name: 'hotels' })
export class HotelStore extends EntityStore<HotelState, Hotel> {
  constructor() {
    super();
  }
}
