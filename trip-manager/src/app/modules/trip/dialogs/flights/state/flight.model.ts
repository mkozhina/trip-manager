import { Flight } from './flight.model';
import { ID } from '@datorama/akita';

export interface Flight {
  id: ID;
  tripID: string;
  passenger: string;
  className: string;
  from: string;
  destination: string;
  flightNumber: string;
  gate: string;
  depDate: string;
  arrDate: string;
  terminal: string;
  seat: string;
  depTime: string;
  arrTime: string;
  arrTerminal: string;
  arrGate: number;
}

/**
 * A factory function that creates Flight
 */
export function createFlight({
  id,
  tripID,
  passenger,
  className,
  from,
  destination,
  flightNumber,
  gate,
  depDate,
  arrDate,
  terminal,
  seat,
  depTime,
  arrTime,
  arrTerminal,
  arrGate,
}): Flight {
  return {
    id,
    tripID,
    passenger,
    className,
    from,
    destination,
    flightNumber,
    gate,
    depDate,
    arrDate,
    terminal,
    seat,
    depTime,
    arrTime,
    arrTerminal,
    arrGate,
  };
}
