import { AppConfig } from 'src/app/configs/app.config';
import { NotificationService } from './../../../../../shared/services/notification.service';
import { Injectable } from '@angular/core';
import { Flight } from './flight.model';
import { createFlight } from './flight.model';
import { FlightStore } from './flight.store';
import {
  AngularFirestoreCollection,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Subscription } from 'rxjs';

@Injectable()
export class FlightService {
  private subscriptions = new Subscription();
  public flightsCollection: AngularFirestoreCollection;

  constructor(
    private afs: AngularFirestore,
    private flightStore: FlightStore,
    private notify: NotificationService,
  ) {
    this.flightsCollection = afs.collection(AppConfig.db.flights);
    this.fetch();
  }

  fetch() {
    this.subscriptions.add(
      this.flightsCollection.valueChanges().subscribe((flights: Flight[]) => {
        this.flightStore.set(flights);
      }),
    );
  }

  getItem(id: string) {
    this.fetch();
    this.flightStore.setActive(id);
  }

  addItem(flight: Flight) {
    const id = this.afs.createId();
    flight.id = id;
    this.flightsCollection
      .doc(id)
      .set(flight)
      .then(res => {
        this.flightStore.add(createFlight(flight));
      })
      .catch(err => this.notify.error(err.message));
  }

  delete(id: string) {
    this.flightsCollection
      .doc(id)
      .delete()
      .then(res => {
        this.flightStore.remove(id);
        this.notify.success('Deleted');
      });
  }

  update(id: string, flight: Flight) {
    this.flightsCollection
      .doc(id)
      .update({ ...flight })
      .then(res => {
        this.flightStore.update(id, { ...flight });
        this.notify.success('Flight updated');
      })
      .catch(err => this.notify.error(err.message));
  }

  destroy() {
    this.subscriptions.unsubscribe();
  }
}
