import { Flight } from './flight.model';
import { Injectable } from '@angular/core';
import { Query, QueryEntity } from '@datorama/akita';
import { FlightStore, FlightState } from './flight.store';

@Injectable({ providedIn: 'root' })
export class FlightQuery extends QueryEntity<FlightState, Flight> {
  constructor(protected store: FlightStore) {
    super(store);
  }
}
