import { Injectable } from '@angular/core';
import { Flight } from './flight.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface FlightState extends EntityState<Flight> {}
@Injectable({
  providedIn: 'root',
})
@StoreConfig({ name: 'flights' })
export class FlightStore extends EntityStore<FlightState, Flight> {
  constructor() {
    super();
  }
}
