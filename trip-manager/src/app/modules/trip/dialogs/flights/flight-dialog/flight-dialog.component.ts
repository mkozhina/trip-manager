import { DateFormaterService } from './../../../../../shared/services/date-formater.service';
import { FlightService } from './../state/flight.service';
import { Trip } from './../../../trip.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Flight } from '../state/flight.model';
@Component({
  selector: 'app-flight-dialog',
  templateUrl: './flight-dialog.component.html',
  styleUrls: ['./flight-dialog.component.scss'],
})
export class FlightDialogComponent implements OnInit {
  public startDate: Date;
  public flightForm: FormGroup;
  public depDate: Date;
  public arrDate: Date;
  public editFlight = false;
  constructor(
    public dialogRef: MatDialogRef<FlightDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { trip: Trip; flight: Flight },
    private fb: FormBuilder,
    private flightService: FlightService,
    private dateUtil: DateFormaterService,
  ) {}
  ngOnInit(): void {
    this.createForm(this.data.flight);
  }

  createForm(flight: Flight = null) {
    if (flight) {
      this.editFlight = true;
    }
    this.flightForm = this.fb.group({
      passenger: new FormControl(flight ? flight.passenger : ''),
      className: new FormControl(flight ? flight.className : ''),
      from: new FormControl(flight ? flight.from : ''),
      destination: new FormControl(flight ? flight.destination : ''),
      flightNumber: new FormControl(flight ? flight.flightNumber : ''),
      gate: new FormControl(flight ? flight.gate : ''),
      arrGate: new FormControl(flight ? flight.arrGate : ''),
      depDate: new FormControl(
        flight
          ? new Date(this.dateUtil.formatDate(this.data.flight.depDate))
          : new Date(this.data.trip.startDate),
      ),
      arrDate: new FormControl(
        flight
          ? new Date(this.dateUtil.formatDate(this.data.flight.arrDate))
          : new Date(this.data.trip.startDate),
      ),
      terminal: new FormControl(flight ? flight.terminal : ''),
      arrTerminal: new FormControl(flight ? flight.arrTerminal : ''),
      seat: new FormControl(flight ? flight.seat : ''),
      depTime: new FormControl(flight ? flight.depTime : ''),
      arrTime: new FormControl(flight ? flight.arrTime : ''),
    });
  }

  dateFilter = (d: Date): boolean => {
    if (this.data.trip) {
      const start = moment(this.data.trip.startDate);
      const end = moment(this.data.trip.endDate);
      const dFormated = moment(d);
      return dFormated >= start && dFormated <= end;
    }
    return true;
  };

  dateStartFilter = (d: Date) => {
    console.log(d);
  };

  onNoClick(): void {
    this.dialogRef.close();
  }

  submit() {
    if (this.data.flight && this.data.flight.id) {
      const id = this.data.flight.id.toString();
      const obj = { id, ...this.flightForm.value };
      this.dialogRef.close(obj);
    } else {
      const tripId = this.data.trip.id;
      const obj = { tripID: tripId, ...this.flightForm.value };
      this.flightService.addItem(obj);
      this.dialogRef.close();
    }
  }
}
