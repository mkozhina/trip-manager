import { FlightQuery } from './../state/flight.query';
import { FlightService } from './../state/flight.service';
import { Flight } from './../state/flight.model';
import { Component, OnInit, Input } from '@angular/core';
import { DeleteDialogComponent } from '../../delete-dialog/delete-dialog.component';
import { MatDialog } from '@angular/material';
import { FlightDialogComponent } from '../flight-dialog/flight-dialog.component';

@Component({
  selector: 'app-flight-view',
  templateUrl: './flight-view.component.html',
  styleUrls: ['./flight-view.component.scss'],
})
export class FlightViewComponent implements OnInit {
  @Input() flight: Flight;
  constructor(
    public dialog: MatDialog,
    public flightService: FlightService,
    public flightQuery: FlightQuery,
  ) {}

  ngOnInit() {}

  deleteFlight(id: any) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: id,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.flightService.delete(result);
      }
    });
  }

  editFlight(id): void {
    const dialogRef = this.dialog.open(FlightDialogComponent, {
      width: '1000px',
      data: { flight: this.flight },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.flightService.update(result.id, { ...result });
      }
    });
  }
}
