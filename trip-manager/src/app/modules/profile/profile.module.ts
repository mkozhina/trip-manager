import { NgModule } from '@angular/core';
import { SharedModule } from './../../shared/shared.module';
import { ProfileRoutingModule } from './profile.routing';
import { ProfileComponent } from './profile/profile.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ProfileComponent],
  imports: [ProfileRoutingModule, SharedModule, CommonModule],
})
export class ProfileModule {}
