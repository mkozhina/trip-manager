import { NotificationService } from './../../../shared/services/notification.service';
import { AuthService } from './../../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import * as firebase from 'firebase';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  public name = '';
  public path = '';
  public profileForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notify: NotificationService,
  ) {
    this.profileForm = this.fb.group({
      name: new FormControl(this.name, [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.authService.user.subscribe(res => {
      if (res) {
        this.createForm(res);
      }
    });
  }

  createForm(res: firebase.User) {
    this.name = res.displayName;
    this.path = res.photoURL;
    this.profileForm = this.fb.group({
      name: [res.displayName, Validators.required],
    });
  }

  updateName() {
    this.authService
      .updateProfile(this.profileForm.value.name)
      .subscribe(() => {
        this.notify.success('Updated');
      });
  }

  handleImageUpload(imageUrl: string) {
    this.path = imageUrl;
    this.authService.updateProfile(this.name, imageUrl).subscribe(() => {
      this.notify.success('Updated');
    });
  }
}
