import { GoogleCalendarService } from './services/google-calendar.service.ts.service';
import { AuthService } from './services/auth.service';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoaderComponent } from './components/loader/loader.component';
import { FirebaseModule } from './modules/firebase.module';
import { MaterialModule } from './modules/material.module';
import { UploaderComponent } from './components/uploader/uploader.component';
import { MenuComponent } from './components/menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { DialogMarkerComponent } from './components/map/dialog-marker/dialog-marker.component';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    HomePageComponent,
    LoaderComponent,
    UploaderComponent,
    MenuComponent,
    DialogMarkerComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD2lNNalCT6plyeFUbmJVQ8qo-Fle-9vHs',
    }),
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    MaterialModule,
    LoaderComponent,
    RouterModule,
    FirebaseModule,
    FormsModule,
    ReactiveFormsModule,
    UploaderComponent,
    MenuComponent,
    HttpClientModule,
    AgmCoreModule,
    DialogMarkerComponent,
  ],
  entryComponents: [DialogMarkerComponent],
  providers: [AuthService, GoogleCalendarService],
})
export class SharedModule {}
