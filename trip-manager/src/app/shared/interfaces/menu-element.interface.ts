export interface MenuElement {
  link: string;
  action: string;
}
