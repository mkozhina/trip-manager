import { AppConfig } from 'src/app/configs/app.config';
import { NotificationService } from './../../services/notification.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as firebase from 'firebase';
import { LoaderService } from '../loader/loader.service';

class FileSnippet {
  constructor(public src: string, public file: File) {}
}

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
})
export class UploaderComponent implements OnInit {
  @Output() imageUploaded = new EventEmitter();
  @Input() path = '';

  public selectedFile: FileSnippet;
  public fileChangedEvent: any;
  public uploaded = true;
  public uploadTask: firebase.storage.UploadTask;

  constructor(
    private notify: NotificationService,
    public domSanitizationService: DomSanitizer,
    private loaderService: LoaderService,
  ) {}

  ngOnInit() {
    this.selectedFile = new FileSnippet(this.path, null);
  }

  onFileChanged(event) {
    this.uploaded = false;
    this.selectedFile = event.target.files[0];

    if (event.target.files[0]) {
      const URL = window.URL;
      let file;
      let img;
      file = event.target.files[0];
      if (file.type === 'image/png' || file.type === 'image/jpeg') {
        img = new Image();

        const self = this;
        img.src = URL.createObjectURL(file);
        self.selectedFile = new FileSnippet(img.src, file);
      } else {
        this.notify.error(
          'Unsupported File Type. Only jpeg and png is allowed!',
        );
      }
    }
  }

  onUpload() {
    if (this.selectedFile) {
      const reader = new FileReader();

      reader.addEventListener('load', (event: any) => {
        this.selectedFile.src = event.target.result;

        const formData = new FormData();
        const storageReference = firebase
          .storage()
          .ref(`/${AppConfig.db.images}/` + this.selectedFile.file.name);
        this.uploadTask = storageReference.put(this.selectedFile.file);

        formData.append('image', this.selectedFile.file);

        this.uploadTask.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          snapshot => {
            this.showLoader();
          },
          error => {
            this.notify.error('Error during uploading file or image!'),
              this.hideLoader();
          },
          () => {
            this.uploadTask.snapshot.ref.getDownloadURL().then(
              downloadURL => {
                this.imageUploaded.emit(downloadURL);
                this.selectedFile = new FileSnippet(downloadURL, null);
                this.uploaded = true;
                this.hideLoader();
              },
              err => {
                this.notify.error('Error during uploading file or image!'),
                  this.hideLoader();
              },
            );
          },
        );
      });

      reader.readAsDataURL(this.selectedFile.file);
    }
  }

  private showLoader(): void {
    this.loaderService.show();
  }
  private hideLoader(): void {
    this.loaderService.hide();
  }
}
