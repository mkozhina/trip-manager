import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
export interface DialogData {
  destination: '';
}
@Component({
  selector: 'app-dialog-marker',
  templateUrl: './dialog-marker.component.html',
})
export class DialogMarkerComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogMarkerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
