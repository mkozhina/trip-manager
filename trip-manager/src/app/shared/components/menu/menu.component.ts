import { MENU_DATA_EDIT } from './../../constants/menu-data.constants';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  @Input() id: string;
  public menuData: { link: string; action: string; id: string }[];
  constructor() {}

  ngOnInit() {
    const id = this.id;
    this.menuData = MENU_DATA_EDIT.map(m => {
      return { id, ...m };
    });
  }
}
