import { NgModule } from "@angular/core";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSnackBarModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatDividerModule,
  MatButtonToggleModule
} from "@angular/material";
import { CdkTreeModule } from "@angular/cdk/tree";
import { CdkTableModule } from "@angular/cdk/table";
import { CdkStepperModule } from "@angular/cdk/stepper";
import { ScrollDispatchModule } from "@angular/cdk/scrolling";
import { PortalModule } from "@angular/cdk/portal";
import { PlatformModule } from "@angular/cdk/platform";
import { OverlayModule } from "@angular/cdk/overlay";
import { ObserversModule } from "@angular/cdk/observers";
import { BidiModule } from "@angular/cdk/bidi";
import { A11yModule } from "@angular/cdk/a11y";

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatSliderModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    MatInputModule,
    MatGridListModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatListModule,
    MatDialogModule,
    MatSidenavModule,
    MatSelectModule,
    MatCheckboxModule,
    MatChipsModule,
    MatRadioModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatDividerModule,
    MatButtonToggleModule
  ],
  exports: [
    // CDK
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    ScrollDispatchModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,

    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatSliderModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    MatInputModule,
    MatGridListModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatListModule,
    MatDialogModule,
    MatTableModule,
    MatSidenavModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRippleModule,
    MatChipsModule,
    MatTabsModule,
    MatRadioModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatDividerModule,
    MatButtonToggleModule
  ]
})
export class MaterialModule {}
