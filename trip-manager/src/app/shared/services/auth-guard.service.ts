import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AppConfig } from '../../configs/app.config';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate([AppConfig.routes.auth_login]);
      return false;
    }
    return true;
  }
}
