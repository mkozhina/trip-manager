import { AuthService } from './auth.service';
import { NotificationService } from './notification.service';
import { Injectable } from '@angular/core';
import { from } from 'rxjs';

@Injectable()
export class GoogleCalendarService {
  constructor(
    private notify: NotificationService,
    private authService: AuthService,
  ) {}

  public addEventToCalendar(startDate, endDate, title) {
    const reqBody = {
      summary: title,
      description: title,
      start: {
        dateTime: startDate,
        // timeZone: 'America/Los_Angeles',
      },
      end: {
        dateTime: endDate,
        //  timeZone: 'America/Los_Angeles',
      },
    };

    from(this.authService.initGoogleClient()).subscribe(res => {
      gapi.client['calendar'].events
        .insert({
          calendarId: 'primary',
          resource: reqBody,
        })
        .then(() => this.notify.success('Calendars event has been added.'));
    });
  }
}
