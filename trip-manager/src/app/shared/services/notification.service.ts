import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";

@Injectable({ providedIn: "root" })
export class NotificationService {
  constructor(private snackBar: MatSnackBar) {}
  success(message: string) {
    this.snackBar.open(message, "Close", {
      panelClass: ["primary"],
      duration: 2000
    });
  }

  warn(message: string) {
    this.snackBar.open(message, "Close", {
      panelClass: ["warn"],
      duration: 2000
    });
  }

  error(message: string) {
    this.snackBar.open(message, "Close", {
      panelClass: ["error"],
      duration: 2000
    });
  }
}
