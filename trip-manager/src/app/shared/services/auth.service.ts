import { AppConfig } from 'src/app/configs/app.config';
import { NotificationService } from './notification.service';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { from, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoaderService } from '../components/loader/loader.service';
import { Router } from '@angular/router';
import { resolve } from 'q';

@Injectable()
export class AuthService {
  private currentUserKey = environment.current_user;
  public user: Observable<firebase.User>;
  public googleAuth: gapi.auth2.GoogleAuth;

  constructor(
    public afAuth: AngularFireAuth,
    private loaderService: LoaderService,
    private notify: NotificationService,
    private router: Router,
  ) {
    this.user = afAuth.authState;
  }

  isAuthenticated() {
    return localStorage.getItem(this.currentUserKey) !== null;
  }

  getCurrentUserID() {
    return localStorage.getItem(this.currentUserKey);
  }

  initGoogleClient() {
    return new Promise((resolve, reject) => {
      gapi.load('client:auth2', () => {
        return gapi.client.init(environment.firebase_api).then(() => {
          this.googleAuth = gapi.auth2.getAuthInstance();

          return this.googleAuth
            .signIn({
              prompt: 'consent',
            })
            .then((googleUser: gapi.auth2.GoogleUser) => {
              resolve();
            });

          resolve();
        });
      });
    });
  }

  updateProfile(name: string, avatar: string = '') {
    let fn = null;
    if (avatar) {
      fn = this.afAuth.auth.currentUser.updateProfile({
        displayName: name,
        photoURL: avatar,
      });
    } else {
      fn = this.afAuth.auth.currentUser.updateProfile({
        displayName: name,
      });
    }

    return from(
      new Promise<any>((resolve, reject) => {
        fn.then(() => resolve(), err => reject(err));
      }),
    ).pipe(catchError(err => throwError(err)));
  }

  doRegister(email: string, password: string, name: string) {
    this.showLoader();
    return from(
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          this.hideLoader();
          return this.updateProfile(name);
        })
        .then(() => this.doLogin(email, password)),
    ).pipe(
      catchError(err => {
        this.hideLoader();
        this.notify.error(err.message);
        return throwError(err);
      }),
    );
  }

  doLogin(email: string, password: string) {
    this.showLoader();
    return from(
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          localStorage.setItem(this.currentUserKey, res.user.uid.toString());
          this.hideLoader();
        })
        .catch(err => {
          this.hideLoader();
          this.notify.error(err.message);
        }),
    );
  }

  doLogout() {
    return from(
      this.afAuth.auth
        .signOut()
        .then(res => {
          localStorage.removeItem(this.currentUserKey);
          return this.router.navigate([AppConfig.routes.auth_login]);
        })
        .catch(err => {
          this.notify.error(err.message);
        }),
    );
  }

  private showLoader(): void {
    this.loaderService.show();
  }
  private hideLoader(): void {
    this.loaderService.hide();
  }

  destroy() {}
}
