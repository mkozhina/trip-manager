import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class DateFormaterService {
  constructor() {}

  formatDate(d: any) {
    return d['seconds']
      ? moment.unix(d['seconds']).format('MM/DD/YYYY')
      : moment(d).format('MM/DD/YYYY');
  }
}
