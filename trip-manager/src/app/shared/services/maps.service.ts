import { Coords } from './../interfaces/coords.interface';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MapsService {
  constructor() {}

  getCurrentLocation() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        return { lng: +pos.coords.longitude, lat: +pos.coords.latitude };
      });
    }
  }
}
