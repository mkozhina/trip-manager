import { Hotel } from './../../modules/trip/dialogs/hotels/state/hotel.model';
import { Flight } from './../../modules/trip/dialogs/flights/state/flight.model';
import { Trip } from './../../modules/trip/trip.model';
import { Moment } from 'moment';

export class TripViewList {
  trip: Trip;
  activePeriod: ActivePeriod[];
}
export class Plans {
  flights: Flight[];
  hotels: Hotel[];
}

export class ActivePeriod {
  id: string;
  day: Moment;
  plans: Plans;

  constructor(id: string, day: Moment, plans: Plans) {
    this.id = id;
    this.day = day;
    this.plans = plans ? plans : null;
  }
}
