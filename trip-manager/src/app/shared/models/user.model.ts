export class UserModel {
  uid: any;
  image: string;
  name: string;
  provider: string;
  email: string;
  password: any;

  constructor() {
    this.image = "";
    this.name = "";
    this.provider = "";
    this.uid = null;
    this.email = "";
    this.password = null;
  }
}
