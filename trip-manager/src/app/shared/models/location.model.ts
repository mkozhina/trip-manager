import { Coords } from '../interfaces/coords.interface';

export class Location implements Coords {
  lat: number;
  lng: number;
  name: string;

  constructor(location: any = {}) {
    this.name = location.address || '';
    this.lat = location.dest_location ? location.dest_location.lat : 0;
    this.lng = location.dest_location ? location.dest_location.lng : 0;
  }
}
