import { AbstractControl } from '@angular/forms';

export function emailValidator(control: AbstractControl) {
  const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  const isValid = EMAIL_REGEXP.test(control.value);
  if (!isValid) {
    return { invalidEmail: 'Not a valid email.' };
  }
  return null;
}
