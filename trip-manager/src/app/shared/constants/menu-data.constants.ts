import { AppConfig } from 'src/app/configs/app.config';
import { MenuElement } from '../interfaces/menu-element.interface';

export const MENU_DATA_EDIT: MenuElement[] = [
  { link: AppConfig.routes.trip_edit, action: 'Edit' },
];
